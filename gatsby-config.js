module.exports = {
  pathPrefix: 'combat-calculator',
  siteMetadata: {
    title: `Ars Magica 4th Edition Combat Statistics Calculator`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
  ],
}

import React from 'react'

import Weapons from '../data/weapons'

export default class Specialization extends React.Component {
  constructor(props) {
    super(props)
    this.state = { value: 'null' }
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(e) {
    this.setState({ value: e.target.value })
    this.props.onSpecializationChange(this.props.skill, e.target.value)
  }

  render() {
    const skill = this.props.skill
    const specs = Weapons.filter((weapon) => weapon.skill === skill).map(
      (s) => (
        <option key={`${skill}_${s.weapon}`} value={s.weapon}>
          {s.weapon}
        </option>
      )
    )
    return (
      <select
        name={`${skill}_specialization`}
        onChange={this.handleChange}
        value={this.state.value}
      >
        <option value="null">-----</option>
        {specs}
      </select>
    )
  }
}

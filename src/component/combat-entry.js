import React from 'react'

const CombatEntry = (props) => {
  const spec_key = `${props.weapon.skill}_specialization`
  const skill_num = Number(props.character[props.weapon.skill])
  const spec =
    skill_num > 0 &&
    spec_key in props.character &&
    props.character[spec_key] === props.weapon.weapon
      ? 1
      : 0
  const skill = skill_num + spec
  const qik = Number(props.character.quickness)
  const per = Number(props.character.perception)
  const str = Number(props.character.strength)
  const dex = Number(props.character.dexterity)
  const size = Number(props.character.size)
  const enc = Math.trunc(Math.min(Number(props.character.load) + str, 0))
  const damage = (props.weapon.ranged ? 0 : str + size) + props.weapon.dmg + enc
  const def = Number(props.weapon.dfn)
  const defense = isNaN(def) ? props.weapon.dfn : qik + skill + def + enc - size
  const atk = (props.weapon.ranged ? per : dex) + skill + props.weapon.atk + enc
  // TODO: add check for min str
  // TODO: add enc
  // TODO: add injury/fatigue
  return (
    <tr>
      <td>{props.weapon.weapon}</td>
      <td>{qik + skill + props.weapon.init + enc}</td>
      <td>{atk + enc}</td>
      <td>{defense}</td>
      <td>{damage}</td>
      <td>{props.weapon.effRn}</td>
      <td>{props.weapon.spc}</td>
      <td>{props.weapon.str}</td>
    </tr>
  )
}

export default CombatEntry

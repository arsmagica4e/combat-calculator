import React from 'react'

import CombatTable from './combat-table'
import Specialization from './specialization'
import Skills from '../data/skills'
import Weapons from '../data/weapons'

const CHARACTERISTICS = [
  'Intelligence',
  'Perception',
  'Strength',
  'Stamina',
  'Presence',
  'Communication',
  'Dexterity',
  'Quickness',
]

export default class Character extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      intelligence: 0,
      perception: 0,
      strength: 0,
      stamina: 0,
      presence: 0,
      communication: 0,
      dexterity: 0,
      quickness: 0,
      size: 0,
      load: 0,
      brawling: 0,
      brawling_specialization: 'null',
      single: 0,
      single_specialization: 'null',
      board: 0,
      board_specialization: 'null',
      two: 0,
      two_specialization: 'null',
      great: 0,
      great_specialization: 'null',
      chain: 0,
      chain_specialization: 'null',
      shaft: 0,
      shaft_specialization: 'null',
      thrown: 0,
      thrown_specialization: 'null',
      bow: 0,
      bow_specialization: 'null',
      xbow: 0,
      xbow_specialization: 'null',
    }
    /*for (const charatt in CHARACTERISTICS) {
      this.state[charatt.toLowerCase()] = 0;
    }
    for (const key in Object.keys(Skills)) {
      this.state[key] = 0;
    }*/

    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleSpecializationChange = this.handleSpecializationChange.bind(this)
  }

  handleInputChange(event) {
    const target = event.target
    const value = Number(target.value)
    const name = target.name
    console.log(name, value)
    this.setState({
      [name]: value,
    })
  }
  handleSpecializationChange(skill, weapon_name) {
    console.log(skill, weapon_name)
    this.setState({ [`${skill}_specialization`]: weapon_name })
  }

  render() {
    const characteristics = CHARACTERISTICS.map((c) => {
      const name = c.toLowerCase()
      const value = this.state[name]
      return (
        <div key={name}>
          <label>
            {c}
            <input
              name={name}
              type="number"
              max="5"
              min="-5"
              step="1"
              placeholder="0"
              value={value}
              onChange={this.handleInputChange}
            />
          </label>
        </div>
      )
    })
    const character = this.state
    const weapons = Weapons
    const skills = Object.entries(Skills).map(([k, v]) => (
      <div key={k}>
        <label>
          {v}
          <input
            type="number"
            name={k}
            value={this.state[k]}
            min="0"
            step="1"
            onChange={this.handleInputChange}
          />
          <Specialization
            skill={k}
            onSpecializationChange={this.handleSpecializationChange}
          ></Specialization>
        </label>
      </div>
    ))
    const enc = Math.min(Math.trunc(character.load + character.strength), 0)
    return (
      <div>
        <div>{characteristics}</div>
        <hr />
        <div>{skills}</div>
        <hr />
        <div>
          <label>
            Size
            <input
              type="number"
              name="size"
              value={this.state.size}
              onChange={this.handleInputChange}
              min="-5"
              max="5"
              step="1"
            />
          </label>
        </div>
        <div>
          <label>
            Load
            <input
              type="number"
              name="load"
              value={this.state.load}
              onChange={this.handleInputChange}
              max="0"
              step="0.5"
            />
          </label>
        </div>
        <CombatTable character={character} weapons={weapons} />
        <hr />
        <div>Encumberance: {enc}</div>
      </div>
    )
  }
}

import React from 'react'
import CombatEntry from './combat-entry'

const CombatTable = (props) => {
  const character = props.character
  const weapons = props.weapons.map((w) => (
    <CombatEntry key={w.weapon} weapon={w} character={character} />
  ))
  return (
    <table>
      <thead>
        <tr>
          <th>Weapon</th>
          <th>Initiative</th>
          <th>Attack</th>
          <th>Defense</th>
          <th>Damage</th>
          <th>Range</th>
          <th>Space</th>
          <th>Minimum Strength</th>
        </tr>
      </thead>
      <tbody>{weapons}</tbody>
    </table>
  )
}

export default CombatTable

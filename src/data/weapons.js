const Weapons = [
  // Brawling
  {
    weapon: 'Brawling',
    skill: 'brawling',
    init: 1,
    atk: 0,
    dfn: 0,
    dmg: 0,
    str: 'n/a',
    load: 0.0,
    spc: 0,
    effRn: 'Touch',
    cost: 'n/a',
    ranged: false,
  },
  {
    weapon: 'Gauntlet',
    skill: 'brawling',
    init: 1,
    atk: 0,
    dfn: 1,
    dmg: 1,
    str: 'n/a',
    load: 0.0,
    spc: 0,
    effRn: 'Touch',
    cost: 'inexpensive',
    ranged: false,
  },
  {
    weapon: 'Knife/Sap',
    skill: 'brawling',
    init: 1,
    atk: 0,
    dfn: 1,
    dmg: 2,
    str: 'n/a',
    load: 0.0,
    spc: 0,
    effRn: 'Touch',
    cost: 'inexpensive',
    ranged: false,
  },
  {
    weapon: 'Dagger',
    skill: 'brawling',
    init: 2,
    atk: 1,
    dfn: 2,
    dmg: 3,
    str: 'n/a',
    load: 0.0,
    spc: 0,
    effRn: 'Touch',
    cost: 'inexpensive',
    ranged: false,
  },
  // Single
  {
    weapon: 'Shortspear',
    skill: 'single',
    init: 5,
    atk: 1,
    dfn: 2,
    dmg: 3,
    str: -1,
    load: -0.5,
    spc: 1,
    effRn: 'Reach',
    cost: 'inexpensive',
    ranged: false,
  },
  {
    weapon: 'Mace',
    skill: 'single',
    init: 3,
    atk: 2,
    dfn: 3,
    dmg: 5,
    str: -1,
    load: -0.5,
    spc: 1,
    effRn: 'Reach',
    cost: 'inexpensive',
    ranged: false,
  },
  {
    weapon: 'Axe',
    skill: 'single',
    init: 3,
    atk: 1,
    dfn: 2,
    dmg: 6,
    str: 0,
    load: -0.5,
    spc: 1,
    effRn: 'Reach',
    cost: 'standard',
    ranged: false,
  },
  {
    weapon: 'Shortsword',
    skill: 'single',
    init: 4,
    atk: 2,
    dfn: 3,
    dmg: 3,
    str: -2,
    load: -0.5,
    spc: 1,
    effRn: 'Reach',
    cost: 'standard',
    ranged: false,
  },
  {
    weapon: 'Longsword',
    skill: 'single',
    init: 3,
    atk: 1,
    dfn: 4,
    dmg: 4,
    str: -1,
    load: -0.5,
    spc: 1,
    effRn: 'Reach',
    cost: 'standard',
    ranged: false,
  },
  // Shield
  // Chain
  {
    weapon: 'Net',
    skill: 'chain',
    init: 0,
    atk: 4,
    dfn: 2,
    dmg: 0,
    str: -2,
    load: -0.5,
    spc: 2,
    effRn: 'Reach',
    cost: 'inexpensive',
    ranged: false,
  },
  {
    weapon: 'Whip',
    skill: 'chain',
    init: 0,
    atk: 6,
    dfn: 0,
    dmg: 2,
    str: -2,
    load: -0.5,
    spc: 4,
    effRn: 'Reach',
    cost: 'standard',
    ranged: false,
  },
  {
    weapon: 'Morningstar (2h)',
    skill: 'chain',
    init: 1,
    atk: 5,
    dfn: 0,
    dmg: 8,
    str: 1,
    load: -0.5,
    spc: 3,
    effRn: 'Reach',
    cost: 'standard',
    ranged: false,
  },
  {
    weapon: 'Flail (2h)',
    skill: 'chain',
    init: 2,
    atk: 4,
    dfn: 2,
    dmg: 0,
    str: -2,
    load: -0.5,
    spc: 2,
    effRn: 'Reach',
    cost: 'inexpensive',
    ranged: false,
  },
  // Two
  {
    weapon: 'Cloak/Dagger',
    skill: 'two',
    init: 6,
    atk: 3,
    dfn: 4,
    dmg: 2,
    str: -2,
    load: -0.5,
    spc: 2,
    effRn: 'Reach',
    cost: 'inexpensive',
    ranged: false,
  },
  {
    weapon: 'Shortsword/Dagger',
    skill: 'two',
    init: 6,
    atk: 5,
    dfn: 4,
    dmg: 5,
    str: 0,
    load: -1.0,
    spc: 1,
    effRn: 'Reach',
    cost: 'standard',
    ranged: false,
  },
  {
    weapon: 'Two shortswords',
    skill: 'two',
    init: 5,
    atk: 4,
    dfn: 5,
    dmg: 5,
    str: 1,
    load: -1.0,
    spc: 1,
    effRn: 'Reach',
    cost: 'standard',
    ranged: false,
  },
  {
    weapon: 'Two axes',
    skill: 'two',
    init: 4,
    atk: 3,
    dfn: 3,
    dmg: 7,
    str: 1,
    load: -1.5,
    spc: 1,
    effRn: 'Reach',
    cost: 'standard',
    ranged: false,
  },
  {
    weapon: 'Trident/Net',
    skill: 'two',
    init: 6,
    atk: 3,
    dfn: 4,
    dmg: 4,
    str: 0,
    load: -1.0,
    spc: 1,
    effRn: 'Reach',
    cost: 'expensive',
    ranged: false,
  },
  // Great Weapons
  {
    weapon: 'Quarterstaff',
    skill: 'great',
    init: 6,
    atk: 4,
    dfn: 6,
    dmg: 3,
    str: 0,
    load: -1.0,
    spc: 3,
    effRn: 'Reach',
    cost: 'inexpensive',
    ranged: false,
  },
  {
    weapon: 'Greatsword',
    skill: 'great',
    init: 6,
    atk: 4,
    dfn: 4,
    dmg: 8,
    str: 1,
    load: -1.0,
    spc: 2,
    effRn: 'Reach',
    cost: 'expensive',
    ranged: false,
  },
  {
    weapon: 'Polearm',
    skill: 'great',
    init: 6,
    atk: 3,
    dfn: 5,
    dmg: 9,
    str: 1,
    load: -1.5,
    spc: 3,
    effRn: 'Reach',
    cost: 'expensive',
    ranged: false,
  },
  {
    weapon: 'War Maul',
    skill: 'great',
    init: 5,
    atk: 2,
    dfn: 2,
    dmg: 10,
    str: 1,
    load: -1.5,
    spc: 2,
    effRn: 'Reach',
    cost: 'standard',
    ranged: false,
  },
  {
    weapon: 'Battleaxe',
    skill: 'great',
    init: 5,
    atk: 3,
    dfn: 3,
    dmg: 10,
    str: 1,
    load: -1.5,
    spc: 2,
    effRn: 'Reach',
    cost: 'expensive',
    ranged: false,
  },
  // Longshaft weapons
  {
    weapon: 'Spear/Lance',
    skill: 'shaft',
    init: 5,
    atk: 6,
    dfn: 1,
    dmg: 6,
    str: -1,
    load: -1.0,
    spc: 3,
    effRn: 'Close',
    cost: 'inexpensive',
    ranged: false,
  },
  {
    weapon: 'Pike',
    skill: 'shaft',
    init: 6,
    atk: 4,
    dfn: 1,
    dmg: 6,
    str: 0,
    load: -1.5,
    spc: 4,
    effRn: 'Close',
    cost: 'standard',
    ranged: false,
  },
  {
    weapon: 'Billhook',
    skill: 'shaft',
    init: 5,
    atk: 6,
    dfn: 1,
    dmg: 8,
    str: 0,
    load: -1.5,
    spc: 3,
    effRn: 'Reach',
    cost: 'inexpensive',
    ranged: false,
  },
  // Thrown
  {
    weapon: 'Rock',
    skill: 'thrown',
    init: 4,
    atk: 0,
    dfn: 'n/a',
    dmg: 2,
    str: 'n/a',
    load: 0.0,
    spc: 0,
    effRn: 'Near',
    cost: 'n/a',
    ranged: true,
  },
  {
    weapon: 'Sling',
    skill: 'thrown',
    init: 2,
    atk: 2,
    dfn: 'n/a',
    dmg: 3,
    str: 'n/a',
    load: 0.0,
    spc: 1,
    effRn: 'Middle',
    cost: 'inexpensive',
    ranged: true,
  },
  {
    weapon: 'Throwing Knife',
    skill: 'thrown',
    init: 1,
    atk: 1,
    dfn: 'n/a',
    dmg: 3,
    str: 'n/a',
    load: 0.0,
    spc: 0,
    effRn: 'Near',
    cost: 'standard',
    ranged: true,
  },
  {
    weapon: 'Throwing Axe',
    skill: 'thrown',
    init: 1,
    atk: 1,
    dfn: 0,
    dmg: 4,
    str: -1,
    load: 0.5,
    spc: 1,
    effRn: 'Near',
    cost: 'standard',
    ranged: true,
  },
  {
    weapon: 'Javelin',
    skill: 'thrown',
    init: 0,
    atk: 2,
    dfn: 0,
    dmg: 6,
    str: -1,
    load: -1.0,
    spc: 2,
    effRn: 'Near',
    cost: 'standard',
    ranged: true,
  },
  {
    weapon: 'Caber',
    skill: 'thrown',
    init: -5,
    atk: 2,
    dfn: 'n/a',
    dmg: 10,
    str: 3,
    load: -6.0,
    spc: 1,
    effRn: 'Close',
    cost: 'standard',
    ranged: true,
  },
  // Bows
  {
    weapon: 'Short Bow',
    skill: 'bow',
    init: 0,
    atk: 0,
    dfn: 'n/a',
    dmg: 4,
    str: -1,
    load: -0.5,
    spc: 1,
    effRn: 'Far',
    cost: 'standard',
    ranged: true,
  },
  {
    weapon: 'Long Bow',
    skill: 'bow',
    init: -1,
    atk: 1,
    dfn: 'n/a',
    dmg: 10,
    str: 1,
    load: -0.5,
    spc: 1,
    effRn: 'Far',
    cost: 'standard',
    ranged: true,
  },
  {
    weapon: 'Composite Bow',
    skill: 'bow',
    init: -1,
    atk: 1,
    dfn: 'n/a',
    dmg: 9,
    str: 0,
    load: -0.5,
    spc: 1,
    effRn: 'Far',
    cost: 'expensive',
    ranged: true,
  },
  // XBow
  {
    weapon: 'Light Crossbow',
    skill: 'xbow',
    init: -6,
    atk: 2,
    dfn: 'n/a',
    dmg: 10,
    str: -2,
    load: -0.5,
    spc: 1,
    effRn: 'Far',
    cost: 'expensive',
    ranged: true,
  },
  {
    weapon: `Heavy Crossbow with Crank/Goat's foot`,
    skill: 'xbow',
    init: -10,
    atk: 2,
    dfn: 'n/a',
    dmg: 12,
    str: -2,
    load: -1.0,
    spc: 1,
    effRn: 'Far',
    cost: 'expensive',
    ranged: true,
  },
  {
    weapon: 'Heavy Crossbow with Gloves',
    skill: 'xbow',
    init: -6,
    atk: 2,
    dfn: 'n/a',
    dmg: 12,
    str: 2,
    load: -1.0,
    spc: 1,
    effRn: 'Far',
    cost: 'expensive',
    ranged: true,
  },
]

export default Weapons

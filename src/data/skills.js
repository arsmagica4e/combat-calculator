const Skills = {
  brawling: 'Brawling',
  single: 'Single Weapon',
  board: 'Shield & Weapon',
  two: 'Two Weapons',
  great: 'Great Weapons',
  chain: 'Chain Weapons',
  shaft: 'Longshaft Weapon',
  thrown: 'Thrown Weapon',
  bow: 'Bows',
  xbow: 'Crossbows',
}

export default Skills

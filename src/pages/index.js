import React from 'react'
//import Link from 'gatsby-link'
import Character from '../component/character'
import TemplateWrapper from '../layouts/index'

/* <h1>Hi people</h1>
    <p>Welcome to your new Gatsby site.</p>
    <p>Now go build something great.</p>
    <Link to="/page-2/">Go to page 2</Link> */

const IndexPage = () => (
  <TemplateWrapper>
    <Character />
  </TemplateWrapper>
)

export default IndexPage
